export class Size {

  static make([x, y]: number[]): Size {
    return new Size(x, y)
  }

  constructor(
    readonly x: number,
    readonly y: number
  ) {}

  unmake(): number[] {
    return [this.x, this.y]
  }

  isLikeBackSlash(): boolean {
    return Math.sign(this.x) !== Math.sign(this.y)
  }

  isStraight(): boolean {
    return this.x === 0 || this.y === 0
  }

  onlyX(): Size {
    return new Size(this.x, 0)
  }

  onlyY(): Size {
    return new Size(0, this.y)
  }

}
