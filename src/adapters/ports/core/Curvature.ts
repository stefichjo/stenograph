export enum Curvature {
  Straight,
  Left,
  Right
}

export function flip(curvature: Curvature) {
  switch (curvature) {
    case Curvature.Straight: return Curvature.Straight
    case Curvature.Left: return Curvature.Right
    case Curvature.Right: return Curvature.Left
  }
}

