import { Size, Segment, Curvature, flip, Glyph } from "./Domain"

it('can make the sequel of a segment', () => {
  let segment11Right = new Segment(new Size(1, 1), Curvature.Right)
  let segment11RightNexts: Segment[] = [
    new Segment(Size.make([1, 1]), Curvature.Left),
    new Segment(Size.make([-1, 1]), Curvature.Right),
    new Segment(Size.make([-1, -1]), Curvature.Left),
    new Segment(Size.make([1, -1]), Curvature.Right)
  ]

  for (const nextSegment of segment11RightNexts) {
    expect(segment11Right.next(nextSegment.size).curvature).toEqual(nextSegment.curvature)
  }

  let segment11Left = new Segment(new Size(1, 1), Curvature.Left)
  let segment11LeftNexts: Segment[] = [
    new Segment(Size.make([1, 1]), Curvature.Right),
    new Segment(Size.make([-1, 1]), Curvature.Left),
    new Segment(Size.make([-1, -1]), Curvature.Right),
    new Segment(Size.make([1, -1]), Curvature.Left)
  ]

  for (const nextSegment of segment11LeftNexts) {
    expect(segment11Left.next(nextSegment.size).curvature).toEqual(nextSegment.curvature)
  }
    
})

it('can append a size to a glyph', () => {
  let noSegments: Segment[] = []
  expect(
    (new Glyph(noSegments)).append(new Size(0, 1)).segments
  ).toEqual(
    noSegments.concat([new Segment(new Size(0, 1), Curvature.Straight)])
  )

  let straightSegments = [new Segment(new Size(0, 1), Curvature.Straight)]
  expect(
    (new Glyph(straightSegments)).append(new Size(1, 1)).segments
  ).toEqual(
    straightSegments.concat([new Segment(new Size(1, 1), Curvature.Left)])
  )

  let leftSegments = [new Segment(new Size(1, 1), Curvature.Left)]
  expect(
    (new Glyph(leftSegments)).append(new Size(1, 1)).segments
  ).toEqual(
    leftSegments.concat([new Segment(new Size(1, 1), Curvature.Right)])
  )

  let rightSegments = [new Segment(new Size(1, 1), Curvature.Right)]
  expect(
    (new Glyph(rightSegments)).append(new Size(1, 1)).segments
  ).toEqual(
    rightSegments.concat([new Segment(new Size(1, 1), Curvature.Left)])
  )
})

it('can flip  curvatures', () => {
  expect(flip(Curvature.Left)).toEqual(Curvature.Right)
  expect(flip(Curvature.Right)).toEqual(Curvature.Left)
  expect(flip(Curvature.Straight)).toEqual(Curvature.Straight)
})

it('can flip segments', () => {
  expect(
    new Segment(new Size(0, 1), Curvature.Straight).flip()
  ).toEqual(
    new Segment(new Size(0, 1), Curvature.Straight)
  )
  expect(
    new Segment(new Size(1, 1), Curvature.Left).flip()
  ).toEqual(
    new Segment(new Size(1, 1), Curvature.Right)
  )
  expect(
    new Segment(new Size(1, 1), Curvature.Right).flip()
  ).toEqual(
    new Segment(new Size(1, 1), Curvature.Left)
  )
})

it('can flip glyphs', () => {
  let emptyGlyph = new Glyph([])
  expect(emptyGlyph.flip()).toEqual(emptyGlyph)

  expect(
    (new Glyph([
      new Segment(new Size(0, 1), Curvature.Straight),
      new Segment(new Size(1, 1), Curvature.Left)])).flip()
  ).toEqual(
    new Glyph([
      new Segment(new Size(1, 1), Curvature.Right),
      new Segment(new Size(0, 1), Curvature.Straight)])
  )

  expect(
    (new Glyph([
      new Segment(new Size(0, 1), Curvature.Straight),
      new Segment(new Size(1, 1), Curvature.Left)])).flip().flip()
  ).toEqual(
    new Glyph([
      new Segment(new Size(0, 1), Curvature.Straight),
      new Segment(new Size(1, 1), Curvature.Left)])
  )
})

it('can construct a glyph from sizes', () => {
  expect(
    Glyph.make([new Size(0, 1)]).segments
  ).toEqual(
    [new Segment(new Size(0, 1), Curvature.Straight)]
  )
  expect(
    Glyph.make([
      new Size(0, 1),
      new Size(0, 1)]).segments
  ).toEqual(
    [
      new Segment(new Size(0, 1), Curvature.Straight),
      new Segment(new Size(0, 1), Curvature.Straight)]
  )

  expect(
    Glyph.make([
      new Size(0, 1),
      new Size(1, 1)]).segments
  ).toEqual(
    [
      new Segment(new Size(0, 1), Curvature.Straight),
      new Segment(new Size(1, 1), Curvature.Left)]
  )
})

it('...', () => {

  let foo = new Glyph([])

  // exception: new Size(1, 1)
  foo.append(new Size(0, 1))

})