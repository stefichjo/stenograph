import { Size, Glyph } from "./core/Domain"

export class Glyphs {
  static I: Glyph = Glyph.make(
    [
      [  0,  1],
      [  0,  1]
    ].map(Size.make)
  )
  
  static t: Glyph = Glyph.make(
    [
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1]
    ].map(Size.make)
  )
  
  static tr: Glyph = Glyph.make(
    [
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1]
    ].map(Size.make)
  )
  
  static b: Glyph = Glyph.make(
    [
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  1,  1],
      [  1,  -1]
    ].map(Size.make)
  )
  
  static br: Glyph = Glyph.make(
    [
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  1,  1],
      [  1,  -1]
    ].map(Size.make)
  )
  
  static ent: Glyph = Glyph.make(
    [
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  -1,  1]
    ].map(Size.make)
  )
  
  static j: Glyph = Glyph.make(
    [
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  -1,  1]
    ].map(Size.make)
  )
  
  static g: Glyph = Glyph.make(
    [
      [  1, -1],
      [  1,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1]
    ].map(Size.make)
  )
  
  static gr: Glyph = Glyph.make(
    [
      [  1, -1],
      [  1,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1]
    ].map(Size.make)
  )
  
  static wr: Glyph = Glyph.make(
    [
      [ -1,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1]
    ].map(Size.make)
  )
  
  static pr: Glyph = Glyph.make(
    [
      [ -1,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1]
    ].map(Size.make)
  )
  
  static m: Glyph = Glyph.make(
    [
      [  1, -1],
      [  1,  1],
      [  0,  1],
      [  0,  1],
      [  1,  1],
      [  1, -1]
    ].map(Size.make)
  )
  
  static cht: Glyph = Glyph.make(
    [
      [  1, -1],
      [  1,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  1,  1],
      [  1, -1]
    ].map(Size.make)
  )

  static auf: Glyph = Glyph.make(
    [
      [ -1,  1],
      [  0,  1],
      [  0,  1],
      [ -1,  1]
    ].map(Size.make)
  )
  
  static v: Glyph = Glyph.make(
    [
      [ -1,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [ -1,  1]
    ].map(Size.make)
  )
  
  static c: Glyph = Glyph.make(
    [
      [  1, -1],
      [  1,  1],
      [ -1,  1],
      [  1,  1],
      [  0,  1]
    ].map(Size.make)
  )

  static cr: Glyph = Glyph.make(
    [
      [  1, -1],
      [  1,  1],
      [ -1,  1],
      [  1,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1]
    ].map(Size.make)
  )

  static w: Glyph = Glyph.make(
    [
      [ -1,  1],
      [  0,  1],
      [  0,  1],
      [  1,  1],
      [  1, -1]
    ].map(Size.make)
  )

  static p: Glyph = Glyph.make(
    [
      [ -1,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  1,  1],
      [  1, -1]
    ].map(Size.make)
  )

  static h: Glyph = Glyph.make(
    [
      [  1, -1],
      [  1,  1],
      [  0,  1],
      [  0,  1],
      [ -1,  1]
    ].map(Size.make)
  )

  static ch: Glyph = Glyph.make(
    [
      [  1, -1],
      [  1,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [ -1,  1]
    ].map(Size.make)
  )

  static fr: Glyph = Glyph.make(
    [
      [  2, -3],
      [ -1, -1],
      [ -1,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1]
    ].map(Size.make)
  )

  static unter: Glyph = Glyph.make(
    [
      [  2, -3],
      [ -1, -1],
      [ -1,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [ -1,  1]
    ].map(Size.make)
  )

  static f: Glyph = Glyph.make(
    [
      [  2, -3],
      [ -1, -1],
      [ -1,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  1,  1],
      [  1, -1]
    ].map(Size.make)
  )

  static z: Glyph = Glyph.make(
    [
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [ -1,  1],
      [ -1, -1],
      [  2, -3]
    ].map(Size.make)
  )

  static st: Glyph = Glyph.make(
    [
      [ -1,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [ -1,  1],
      [ -1, -1],
      [  2, -3],
    ].map(Size.make)
  )

  static sch: Glyph = Glyph.make(
    [
      [  1, -1],
      [  1,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [  0,  1],
      [ -1,  1],
      [ -1, -1],
      [  2, -3]
    ].map(Size.make)
  )

}

export const exampleGlyphs: Glyph[] = [
  Glyphs.I,

  Glyphs.t,
  Glyphs.tr,

  Glyphs.g,
  Glyphs.b,
  Glyphs.gr,
  Glyphs.br,

  Glyphs.ent,
  Glyphs.wr,
  Glyphs.j,
  Glyphs.pr,

  Glyphs.m,
  Glyphs.cht,

  Glyphs.auf,
  Glyphs.v,

  Glyphs.h,
  Glyphs.w,
  Glyphs.ch,
  Glyphs.p,

  Glyphs.c,
  Glyphs.cr,

  Glyphs.z,
  Glyphs.fr,

  Glyphs.st,
  Glyphs.unter,

  Glyphs.sch,
  Glyphs.f,

]

