import React from 'react';
import './App.css';
import { ExampleGlyphs } from './adapters/Interpretation'

function App() {
  return (
    <ExampleGlyphs/>
  );
}

export default App;
