import { } from './../../../Utils'
import { Curvature } from './Curvature'
import { Size } from  './Size'
import { Segment } from './Segment'

export class Glyph {
  constructor(readonly segments: Segment[]) {}

  // TODO exception: appending to empty glyph
  append(size: Size): Glyph {
    let next: Segment = size.isStraight()
      ? Segment.makeStraight(size)
      : this.segments[this.segments.length - 1].next(size)

    return new Glyph(
      this.segments.concat([next])
    )
  }

  flip(): Glyph {
    return new Glyph(this.segments.reverse().map(segment =>
      segment.flip()
    ))
  }

  load(sizes: Size[]): Glyph {
    const appendSize = (glyph: Glyph, size:  Size) => glyph.append(size)

    return sizes.fold(appendSize, this)
  }
  
  static make(sizes: Size[]): Glyph {
    var [preSizes, straightSizes, postSizes] = sizes.partition(size => size.isStraight())

    return (
      new Glyph(
        straightSizes.map(Segment.makeStraight))
      )
        .load(preSizes.reverse())
        .flip()
        .load(postSizes)
  }
  
}
