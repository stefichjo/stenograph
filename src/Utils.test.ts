import { Enum } from './Utils'

it('can zip', () => {
  expect([1, 2, 3].zip(['a', 'b', 'c']))
    .toEqual([[1, 'a'], [2, 'b'], [3, 'c']])
})

it('can partition', () => {
  expect([-3, -2, -1, 0, 0, 0, 1, 2, 3].partition((n: number): boolean => n == 0))
    .toEqual([[-3, -2, -1], [0, 0, 0], [1, 2, 3]])
})

const add = (a: number, b: number): number => a + b

it('can fold', () => {
  expect([1, 2, 3, 4].reduce(add))
    .toEqual(10)
  expect([1, 2, 3, 4].fold(add, 0))
    .toEqual(10)
})

it('can enumerate', () => {
  expect(Enum.from0To(3)).toEqual([0, 1, 2, 3])
  expect(Enum.fromTo(0, 3)).toEqual([0, 1, 2, 3])
  expect(Enum.fromThenTo(2, 5, 11)).toEqual([2, 5, 8, 11])
})

const square = (a: number): number => a * a
const increment = (a: number): number => a + 1
const squareAndIncrement = increment.compose(square)

it('can compose', () => {
  expect(squareAndIncrement(5)).toEqual(26)
})
