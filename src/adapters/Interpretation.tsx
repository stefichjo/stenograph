import React, { Component } from "react"
import { Enum } from "../Utils"
import { Glyph, Segment, Size, Curvature } from "./ports/core/Domain"
import { exampleGlyphs } from "./ports/Language"

// README https://css-tricks.com/animate-calligraphy-with-svg/

const skewX = "skewX(-22)"

type Ligature = Segment[][]

function cHelper(segment: Segment): number[] {
  let [x, y]: number[] = segment.size.unmake()

  return segment.isAsBackSlashLikeAsItHasARightCurvature()
    ? [0, y]
    : [x, 0]
}

function interpreter(segment: Segment) {
  switch (segment.curvature) {
    case Curvature.Straight:
      return tInterpreter(segment)
    default:
      return cInterpreter(segment)
  }
}

const scale = (n: number) => n * 20

function cInterpreter(segment: Segment) {
  return `c 0 0 ${cHelper(segment).concat([segment.size.x, segment.size.y]).map(scale).join(" ")}`
}

function tInterpreter(segment: Segment) {
  return `t ${[segment.size.x, segment.size.y].map(scale)}`
}

export function withStartingLigature(glyph: Glyph, [search, replace]: Ligature): Glyph {
  return JSON.stringify(glyph.segments.slice(0, search.length)) === JSON.stringify(search)
    ? new Glyph(replace.concat(glyph.segments.slice(search.length)))
    : glyph
}

class Ligatures {
  static w = [
    [
      new Segment(new Size(-1, 1), Curvature.Left),
      new Segment(new Size( 0, 1), Curvature.Straight),
      new Segment(new Size( 0, 1), Curvature.Straight),
      new Segment(new Size( 1, 1), Curvature.Left)
    ], [
      new Segment(new Size(-1.5, 3), Curvature.Left),
      new Segment(new Size( 1, 1), Curvature.Left)
    ]
  ]
  static p = [
    [
      new Segment(new Size(-1, 1), Curvature.Left),
      new Segment(new Size( 0, 1), Curvature.Straight),
      new Segment(new Size( 0, 1), Curvature.Straight),
      new Segment(new Size( 0, 1), Curvature.Straight),
      new Segment(new Size( 0, 1), Curvature.Straight),
      new Segment(new Size( 0, 1), Curvature.Straight),
      new Segment(new Size( 0, 1), Curvature.Straight),
      new Segment(new Size( 1, 1), Curvature.Left)
    ], [
      new Segment(new Size(-1.5, 7), Curvature.Left),
      new Segment(new Size( 1, 1), Curvature.Left)
    ]
  ]
  static wr = [
    [
      new Segment(new Size(-1, 1), Curvature.Left),
      new Segment(new Size( 0, 1), Curvature.Straight),
      new Segment(new Size( 0, 1), Curvature.Straight),
      new Segment(new Size( 0, 1), Curvature.Straight)
    ], [
      new Segment(new Size(-1, 4), Curvature.Left)
    ]
  ]
  static m = [
    [
      new Segment(new Size( 1, -1), Curvature.Right),
      new Segment(new Size( 1,  1), Curvature.Right),
      new Segment(new Size( 0,  1), Curvature.Straight),
      new Segment(new Size( 0,  1), Curvature.Straight),
      new Segment(new Size( 1,  1), Curvature.Left),
      new Segment(new Size( 1, -1), Curvature.Left)
    ], [
      new Segment(new Size( 1, -1), Curvature.Right),
      new Segment(new Size( 1,  1), Curvature.Right),
      new Segment(new Size( -0.5,  2), Curvature.Straight), // TODO :/
      new Segment(new Size( 1,  1), Curvature.Left),
      new Segment(new Size( 1, -1), Curvature.Left),
    ]
  ]
  static foo = [
    [
      new Segment(new Size( 1, -1), Curvature.Right)
    ], [
      new Segment(new Size( 1, -0.5), Curvature.Right)
    ]
  ]

  static ligatures = [
    // this.foo,
    this.wr,
    this.p,
    this.w,
    this.m
  ]


}

export function withLigatures(glyph: Glyph): Glyph {
  const compose = (f: Function, g: Function) => (x: any) => f.compose(g)(x)

  let applyLigatures =
    Ligatures.ligatures
      .map((ligature: Ligature) =>
        (glyph: Glyph) => withStartingLigature(glyph, ligature))
      .reduce(compose)

  return applyLigatures(applyLigatures(glyph).flip()).flip()
}

function renderGlyphs(glyphs: Glyph[]) {
  return glyphs.map(withLigatures)
    .zip(
      Enum.from0To(glyphs.length)
        .map(n => 5 * scale(n))
    ).map(
      (x) => renderGlyph(x[0], x[1])
    )
}

function renderGlyph(glyph: Glyph, x: number) {
  return(
    <path
      d={`
        m ${x} 100
        ${glyph.segments.map(interpreter)}
      `}/>
  );
}

export class ExampleGlyphs extends Component {

  render() {
    return(
      <React.Fragment>
        <svg xmlns="http://www.w3.org/2000/svg" width={ 5 * scale(exampleGlyphs.length) } height="500" stroke="black" stroke-width="5" stroke-linecap="round" fill="none" transform={ skewX }>
          { renderGlyphs(exampleGlyphs) }
        </svg>
      </React.Fragment>
    );
  }
}

