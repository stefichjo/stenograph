declare global {
  interface Array<T> {
    zipWith(op: (a: any, b: any) => any, bs: any[]): any[]
    zip(xs: any[]): any[][]
    fold<A>(append: (_: A, __: T) => A, neutral: A): A
    partition(predicate: ((_: T) => boolean)): T[][]
  }
}

// eslint-disable-next-line no-extend-native
Array.prototype.zipWith = function (op, xs) {
  if (this.length === 0 || xs.length === 0) {
    return []
  } else {
    return ([op(this.shift(), xs.shift())]).concat(this.zipWith(op, xs))
  }
}

// eslint-disable-next-line no-extend-native
Array.prototype.zip = function (xs) {
  const tuple = (a: any, b: any) => [a, b]
  
  return this.zipWith(tuple, xs)
}

// eslint-disable-next-line no-extend-native
Array.prototype.fold = function (append, neutral) {
  let acc = neutral

  for (const element of this) {
    acc = append(acc, element)
  }

  return acc
}

// eslint-disable-next-line no-extend-native
Array.prototype.partition = function (predicate) {
  var pre: any[] = []
  var inter: any[] = []
  var post: any[] = []

  for (const element of this) {
    if (predicate(element)) {
      inter.push(element)
    } else {
      (inter.length === 0 ? pre : post).push(element)
    }
  }

  return [pre, inter, post]
}

export class Enum {
  public static from0To(n: number): number[] {
    return Array.apply(null, Array(n + 1)).map((_: any, x: any) => x)
  }

  public static fromTo(from: number, to: number): number[] {
    return Enum.from0To(to - from).map((n: number) => n + from)
  }

  public static fromThenTo(from: number, then: number, to: number): number[] {
    let step: number = then - from
    let steppingToFrom0: number = to - from
  
    return Enum.from0To(steppingToFrom0 / step)
      .map((n: number) => n * step + from)
  }
}

declare global {
  interface Function {
    compose(g: Function): Function
  }
}

// eslint-disable-next-line no-extend-native
Function.prototype.compose = function (g: Function) {
  return (x: any) => this(g(x))
}
