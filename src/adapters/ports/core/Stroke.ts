import { } from '../../../Utils'
import { Glyph } from './Glyph'

/*

`da` `d+er` `f-er` `wot`

`d` = `d` + ``

Es ist unwichtig, ob man
mit einem evtl. leeren Anstrich
oder mit einem evtl. leeren Glyphen beginnt.

*/

export class Coda {
  constructor(readonly a: any) {}

  // no stroke

}

export class Stroke {
  constructor(readonly glyph: Glyph, readonly coda: Coda) {}

  append(glyph: Glyph, coda: Coda): Stroke {
    return this
  }

}
