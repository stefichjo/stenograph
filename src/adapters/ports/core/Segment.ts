import { Curvature, flip } from './Curvature'
import { Size } from  './Size'

export class Segment {
  constructor(
    readonly size: Size,
    readonly curvature: Curvature) {}
  
  flip() {
    return new Segment(this.size, flip(this.curvature))
  }

  next(size: Size): Segment {
    return new Segment(
      size,
      size.isLikeBackSlash() === this.isLikeHorizontal()
        ? Curvature.Left
        : Curvature.Right)
  }

  isLikeHorizontal(): boolean {
    return this.curvature === Curvature.Straight
      ? this.size.y === 0
      : this.isAsBackSlashLikeAsItHasARightCurvature()
  }

  isAsBackSlashLikeAsItHasARightCurvature(): boolean {
    return this.size.isLikeBackSlash() === (this.curvature === Curvature.Right)
  }

  // TODO exception: non-straight size
  static makeStraight(size: Size): Segment {
    return new Segment(size, Curvature.Straight)
  }

}
